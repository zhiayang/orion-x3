// ares/string.cpp
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.


#include <stdint.h>
#include <string.h>

namespace String
{
	uint64_t Length(const char *str)
	{
		uint64_t retval = 0;
		for(retval = 0; *str != 0; str++)
		{
			retval++;
		}
		return retval;
	}


	char* Copy(char* dest, const char* src)
	{
		char* origdest = dest;
		while(*src)
		{
			*dest++ = *src++;
		}

		*dest = '\0';
		return origdest;
	}

	char* CopyLength(char *dest, const char *src, uint64_t Length)
	{
		char* origdest = dest;
		while(Length)
		{
			*dest++ = *src++;
			Length--;
		}

		*dest = '\0';
		return origdest;
	}

	char* Concatenate(char* dest, const char* src)
	{
		return ConcatenateLength(dest, src, Length(dest) > Length(src) ? Length(dest) : Length(src));
	}

	char* ConcatenateLength(char* dest, const char* src, uint64_t n)
	{
		size_t dest_len = Length(dest);
		size_t i;
		for(i = 0; i < n && src[i] != '\0'; i++)
			dest[dest_len + i] = src[i];

		dest[dest_len + i] = '\0';
		return dest;
	}

	bool Compare(const char* a, const char* b)
	{
		if(Length(a) != Length(b))
		{
			return false;
		}

		for(uint64_t l = (Length(a) > Length(b)) ? Length(a) : Length(b); l > 0; l--)
		{
			if(*(a + l) != *(b + l))
				return false;
		}
		return true;
	}

	char* strrev(char* str, uint64_t length)
	{
		uint64_t i = 0, j = length - 1;
		char tmp;
		while(i < j)
		{
			tmp = str[i];
			str[i] = str[j];
			str[j] = tmp;
			i++;
			j--;
		}

		return str;
	}
}
