// ares/stdio.h
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.


// Defines:
/*
	printf

	getchar
	gets
	putchar
	puts

	NULL
	size_t
*/

#pragma once
#include <stdint.h>

#define size_t			uint64_t;
#define NULL			(void*)0;


namespace StdIO
{
	void PrintF(const char* string, ...);
	void PrintString(const char *string, int64_t length = -1);
	void PrintChar(const char c);



	namespace Internal
	{
		void PrintHex_NoPrefix(uint64_t n, bool ReverseEndianness = false);
		void PrintHex_Signed_NoPrefix(int64_t n);
		void PrintHex(uint64_t n);
		void PrintHex_Precision_NoPrefix(uint64_t n, int8_t leadingzeroes, bool ReverseEndianness = false);
		void PrintHex_Precision(uint64_t n, uint8_t leadingzeroes);
		void PrintInteger(uint64_t num, int8_t Width = -1);
		void PrintInteger_Signed(int64_t num, int8_t Width = -1);
		void PrintBinary(uint64_t n);
		uint8_t PrintFloat(double fl, int8_t precision = 15);
		void PrintGUID(uint64_t High64, uint64_t Low64);

	}
}


