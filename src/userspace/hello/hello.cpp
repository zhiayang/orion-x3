// Hello.cpp
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.

#include <stdio.h>
#include <math.h>

void HelloTest();

extern "C" void HelloStart()
{
	HelloTest();
}

void HelloTest()
{
	// char string[4] = { 'f', 'a', 't', '\n' };

	while(true)
	{
		using namespace StdIO;
		Internal::PrintFloat(Math::SquareRoot(3), 15);
		PrintString("\n");
	}
}



void* mset(void *dst, uint64_t val, unsigned long len)
{
	uintptr_t d0 = 0;

	asm volatile(
		"rep stosq"
		:"=&D" (d0), "+&c" (len)
		:"0" (dst), "a" (val)
		:"memory");

	return dst;
}

