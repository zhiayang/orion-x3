// Boot.s
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// With reference to Sortix: (c) Jonas 'Sortie' Termansen 2011.
// Licensed under Creative Commons Attribution 3.0 Unported


// Sets up Long Mode, Paging and GDT.
// Calls KernelBootStrap (kbootstrap.s)

// Referenced Files:
// src/loader/kbootstrap.s



.global Boot

.section .text
.text 0x00100000
.type Boot, @function












.code32

Boot:
	jmp Prep64


// Multiboot 1 Header
.align 4
MultibootHeader:
	.long 0x1BADB002					// Magic Number
	.long 0x00000003					// Flags	(bits 0, 1)
	.long -(0x1BADB002 + 0x00000003)	// Checksum





Prep64:
	cli
	cld

	// Store multiboot structure pointer.
	// mov %ebx, 0x00400000

	// Store magic value (0x2BADB002)
	mov %eax, 0x0500



	mov %ebx, %esi
	mov $0x40000, %edi
	mov $0x200, %ecx
	rep movsb



	// This is confusing.
	// PML4T (256TB) -> 512x PDPT (512GB) -> 512x PD (1GB) -> 512x PT (2MB) -> 512x Pages (4KB)




	// Clear 0x8000 bytes after 0x1000
	movl $0x1000, %edi
	mov %edi, %cr3
	xorl %eax, %eax
	movl $0x8000, %ecx

	rep stosl
	movl %cr3, %edi




	// Set initial page tables.
	// Will re-create in long mode.


	// OR with 0x3 (R/W, Present)

	// Point the first entry in the PML4T to the first PDPT.

	movl $0x2007, (%edi)		// Make put the address + flags of the PDPT into 0x1000.
	addl $0x1000, %edi			// We only want 1 PDPT, so we add 0x1000 to make it point to the PDPT entries.


	// Point the first PDPT entry to the first PD.

	movl $0x3007, (%edi)		// Same deal, point PDPT entry number one to the PD we create below.
	addl $0x1000, %edi			// Point to the PD now.


	// PD

	movl $0x4007, (%edi)		// Exactly the same as above. Except:
	movl $0x5007, 8(%edi)		// There's a page table at 0x4000, 0x1000 long. There's another one at 0x5000. We address 4MB this way.
	movl $0x6007, 16(%edi)		// 6MB
	movl $0x7007, 24(%edi)		// 8MB
	addl $0x1000, %edi			// Reference the page table at 0x4000.


	// PT
	movl $0x07, %ebx			// Set our flags.
	movl $2048, %ecx			// Since the page tables are contiguous, we can simply loop 2048 times (512 x 4).


	// Memory Map 4MB
SetPTEntry:
	mov %ebx, (%edi)
	add $0x1000, %ebx
	add $8, %edi

	loop SetPTEntry





	// Enable PAE Paging
	mov %cr4, %eax
	orl $0x20, %eax						// Set PAE Bit
	mov %eax, %cr4


	// Enable Long Mode
	mov $0xC0000080, %ecx
	rdmsr
	orl $0x101, %eax
	wrmsr


	// Enable Paging, enter compatibility mode
	mov %cr0, %eax
	orl $0x80000000, %eax
	mov %eax, %cr0

	// Load Long Mode GDT
	mov GDT64Pointer, %eax
	lgdtl GDT64Pointer



	// Jump!
	ljmp $0x8, $Realm64


.code64
Realm64:
	cli

	// Set up segments
	mov $0x10, %ax
	mov %ax, %ds
	mov %ax, %es
	mov %ax, %fs
	mov %ax, %gs


	// Jump to kernel bootstrap!
	jmp Main





Main:
	jmp KernelBootStrap					// kbootstrap.s
	cli
	hlt










































.align 16
// 64-bit GDT
GDT64:
	GDTNull:
		.word 0				// Limit (low)
		.word 0				// Base (low)
		.byte 0				// Base (middle)
		.byte 0				// Access
		.byte 0				// Granularity / Limit (high)
		.byte 0				// Base (high)
	GDTCode:
		.word 0xFFFF		// Limit (low)
		.word 0				// Base (low)
		.byte 0				// Base (middle)
		.byte 0x9A			// Access
		.byte 0xAF			// Granularity / Limit (high)
		.byte 0				// Base (high)
	GDTData:
		.word 0xFFFF		// Limit (low)
		.word 0				// Base (low)
		.byte 0				// Base (middle)
		.byte 0x92			// Access
		.byte 0xAF			// Granularity / Limit (high)
		.byte 0				// Base (high)
	GDTCodeR3:
		.word 0xFFFF		// Limit (low)
		.word 0				// Base (low)
		.byte 0				// Base (middle)
		.byte 0xFA			// Access
		.byte 0xAF			// Granularity / Limit (high)
		.byte 0				// Base (high)
	GDTDataR3:
		.word 0xFFFF		// Limit (low)
		.word 0				// Base (low)
		.byte 0				// Base (middle)
		.byte 0xF2			// Access
		.byte 0xAF			// Granularity / Limit (high)
		.byte 0				// Base (high)
	GDTTSS:
		.word 0x0068		// Limit (low)
		.word 0x0000		// Base (Addr of TSS)
		.byte 0x50			// middle
		.byte 0xE9
		.byte 0x80
		.byte 0x00
		.long 0x00
		.long 0x00



		// Pointer
	GDT64Pointer:
		.word GDT64Pointer - GDT64 - 1	// Limit
		.quad GDT64						// Base




