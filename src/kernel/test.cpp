// test.c
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported

#include <stdlib.h>

using namespace Kernel;
using namespace Library;
using namespace Library::Stdio;



namespace Kernel {
namespace Internal {
namespace Tests
{
	bool PrintTest(int t)
	{
		uint64_t hex = 0x3F7A838;
		int num = 2345712;
		const char* string = "In the event of unexpected shutdown";
		char ch = 'X';
		// doubles only handle 15 decimal places
		double pi = 3.141592653589793;

		printk("\t%da. %wPrinting default hex: %w%x\n", t, VC_Silver, VC_Chartreuse, hex);
		printk("\t%db. %wPrinting leading hex: %w%16x\n", t, VC_Silver, VC_Chartreuse, hex);
		printk("\t%dc. %wPrinting hex without 0x: %w%#x\n", t, VC_Silver, VC_Chartreuse, hex);

		printk("\t%dd. %wPrinting number: %w%d\n", t, VC_Silver, VC_Chartreuse, num);
		printk("\t%de. %wPrinting string: %w%s\n", t, VC_Silver, VC_Chartreuse, string);
		printk("\t%df. %wPrinting character: %w%c\n", t, VC_Silver, VC_Chartreuse, ch);

		num = -num + 9495;
		printk("\t%dg. %wPrinting negative number: %w%+d\n", t, VC_Silver, VC_Chartreuse, num);

		num = -num;
		printk("\t%dh. %wPrinting positive number with sign: %w%+d\n", t, VC_Silver, VC_Chartreuse, num);
		printk("\t%di. %wPrinting pi to 15 decimal places: %w%.15f\n", t, VC_Silver, VC_Chartreuse, pi);

		return true;
	}

	bool MemoryTest(int t)
	{
		printk("\t%da. %wAllocating all memory and performing memset tests %r(may take some time)\n", t, VC_Silver);
		// The next set of operations will consume many CPU cycles; refresh the screen now to let the user know what we're doing
		HAL::Devices::LFB::RefreshBuffer();
		int memtest = 0;
		int smemtest = 0;

		while(!HAL::PMem::IsOutOfMemory())
		{

			// printk("\r\t-- %x", memtest * 0x1000);
			if(memtest == 0)
			{
				smemtest = HAL::AllocatePage() / 0x1000;
				memtest = smemtest;
			}
			else
			{
				memtest = HAL::AllocatePage() / 0x1000;
			}


			Memory::Set64((void*)((uint64_t)memtest * 0x1000), 0xCC, 0x1000 / 8);

			// Clear whatever the previous thing had
			// printk("\r                  ");
		}

		HAL::Devices::LFB::Console::MoveCursor(78, HAL::Devices::LFB::Console::GetCursorY() - 1);
		printk(" -- %wDone %r(Allocated up to %x)\n", VC_Green, memtest * 0x1000);

		// The next set of operations will consume many CPU cycles; refresh the screen now to let the user know what we're doing
		HAL::Devices::LFB::RefreshBuffer();
		printk("\t%db. %wFreeing allocated memory", t, VC_Silver);
		for(int z = memtest; z > smemtest; z--)
		{
			HAL::FreePage(z * 0x1000);
		}

		// DBG_PrintPMemFPL();
		printk(" -- %wDone\n", VC_Green);

		HAL::PMem::CoalesceFPLs();




		// Now we allocate from the heap.
		// This will positively cause us to crash.
		// but let's do it anyway.
		printk("\n");
		while(true)
		{
			void* x = HAL::DMem::AllocateChunk(0x10);
			// printk("%x\n", x);

			Memory::Set((void*)x, 0xFF, 0x10);
			Kernel::HAL::Devices::COM1::WriteString(Library::Utility::ConvertToString((uint64_t)x));
			Kernel::HAL::Devices::COM1::WriteString("\n");
			// HAL::DMem::HeapChunk::DBG_PrintChunks();
			// printk("\r                                ");
		}


		return true;
	}

	bool StringTest(int t)
	{
		// printk("%da. %wstrcpy\n", t, 0xFFAAAAAA);

		// char* str1, str2;
		// str1 = "asdfasdjf";

		// strcpy(str2, str1);
		// printk("%wstr1: %w%s%w, str2: %w%s\n", VC_Silver, VC_Chartreuse, str1, VC_Silver, VC_Chartreuse, str2);

		return true;
	}
}
}
}














