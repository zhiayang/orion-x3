// DynaMem.c
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.


// Heap things.

#include <stdlib.h>

using namespace Kernel;
using namespace Library;
using namespace Library::Stdio;
using namespace Kernel::HAL::DMem;

#define MaximumHeapSizeInPages			0xFFFF
#define ChunkMagic						0xBEEFC0DE



// This stores the address of each slab
// We have slabs of sizes from 1 - 512, making 10 slabs in total -- we might run out of space for these, so it needs to be a pointer to store the addresses of other slabs.
uint64_t* SlabAddresses[10];


uint64_t SizeOfHeapInPages = 1;
uint64_t ChunksInHeap = 0;

uint64_t TopOfHeap = 0;
uint64_t LastFreeAddress = 0;




namespace Kernel {
namespace HAL {
namespace DMem
{
	void* AllocateChunk(uint64_t Size)
	{
		if(Size % sizeof(HeapHeader_type))
			Size = (((Size / sizeof(HeapHeader_type)) + 1) * sizeof(HeapHeader_type));

		if(Size == 0)
			return 0;

		uint64_t ChunkAddr = HeapChunk::FindFirstFreeSlot(Size);

		if(ChunkAddr == -1)
		{
			HeapChunk::ExpandHeap((Size + 0xFFF) / 0x1000);
			return AllocateChunk(Size);
		}
		else
		{
			HeapHeader_type* Header = (HeapHeader_type*)ChunkAddr;

			// sanity checks.
			assert(Header->Magic == ChunkMagic, "Header's magic value corrupted! Check header alignment.");
			assert(Header->Size >= Size, "HeapChunk::FindFirstFreeSlot screwed up!");


			// split if we need to.
			if(Header->Size > Size)
				HeapChunk::SplitChunk(Header, Size);


			// commit the alloc.
			Header->IsFree = false;


			return (void*)(ChunkAddr + sizeof(HeapHeader_type));
		}
	}

	void FreeChunk(void* Pointer)
	{
		// pointer would be a pointer to the memory allocated.
		// get the header

		HeapHeader_type* Header = (HeapHeader_type*)((uint64_t)Pointer - sizeof(HeapHeader_type));

		// sanity check
		assert(Header->Magic == ChunkMagic, "Header's magic value corrupted! Check header alignment.");


		if(Header->IsFree)
		{
			printk("\n\n\n");
			printk("======================================\n");
			printk("|        Stop fooling around!        |\n");
			printk("|  Freeing a free block isn't funny! |\n");
			printk("|              Asshole!              |\n");
			printk("======================================\n");
			printk("\n\n\n");
			return;
		}

		// now the jokes are over...
		// commit the change.
		Header->IsFree = true;

		bool DidCombineLeft = false;
		bool DidCombineRight = false;

		// this is the messy part: coalescing this chunk with the ones on the left and right.
		// so we end up with as large a chunk as possible.

		// check if we're the leftmost (first) chunk.
		if((uint64_t)Header != HeapAddress)
		{
			// good, we can try to combine with the left chunk.
			HeapHeader_type* PrevHeader = Header->PreviousHeader;


			// make sure this is actually the previous header.
			assert(PrevHeader->Magic == ChunkMagic, "Header's magic value corrupted! Check header alignment.");
			assert((uint64_t)PrevHeader + sizeof(HeapHeader_type) + PrevHeader->Size == (uint64_t)Header, "Header misalignment!");

			if(PrevHeader->IsFree)
			{
				// if it's free, combine!
				PrevHeader->Size += sizeof(HeapHeader_type) + Header->Size;
				DidCombineLeft = true;
			}
		}

		// check if we're the rightmost chunk
		if((uint64_t)Header + sizeof(HeapHeader_type) + Header->Size != TopOfHeap)
		{
			// good, we can try to combine with the right chunk.
			HeapHeader_type* NextHeader = (HeapHeader_type*)((uint64_t)Header + sizeof(HeapHeader_type) + Header->Size);

			// sanity check.
			assert(NextHeader->Magic == ChunkMagic, "Header's magic value corrupted! Check header alignment.");
			assert(NextHeader->PreviousHeader == Header, "Previous header mismatch!");

			if(NextHeader->IsFree)
			{
				// it's free, great.
				// absorb it.
				Header->Size += sizeof(HeapHeader_type) + NextHeader->Size;
				DidCombineRight = true;
			}
		}

		// deal with the space here
		if(DidCombineLeft && DidCombineRight)
		{
			// we need to get the left-most header, and increase that size.
			HeapHeader_type* PrevHeader = Header->PreviousHeader;
			HeapHeader_type* NextHeader = (HeapHeader_type*)((uint64_t)Header + sizeof(HeapHeader_type) + Header->Size);



			// sanity check.
			assert(NextHeader->Magic == ChunkMagic, "Header's magic value corrupted! Check header alignment.");
			assert(NextHeader->PreviousHeader == Header, "Previous header mismatch!");


			// make sure this is actually the previous header.
			assert(PrevHeader->Magic == ChunkMagic, "Previous Header's magic value corrupted! Check header alignment.");
			assert((uint64_t)PrevHeader + sizeof(HeapHeader_type) + PrevHeader->Size == (uint64_t)Header, "Previous header mismatch!");

			PrevHeader->Size += sizeof(HeapHeader_type) + NextHeader->Size;
			Memory::Set((void*)((uint64_t)Header), 0x00, sizeof(HeapHeader_type));
		}
		else if(DidCombineLeft)
		{
			HeapHeader_type* PrevHeader = Header->PreviousHeader;


			// make sure this is actually the previous header.
			assert(PrevHeader->Magic == ChunkMagic, "Previous Header's magic value corrupted! Check header alignment.");
			assert((uint64_t)PrevHeader + sizeof(HeapHeader_type) + PrevHeader->Size == (uint64_t)Header, "Previous header mismatch!");

			Memory::Set((void*)((uint64_t)Header), 0x00, sizeof(HeapHeader_type));
		}
	}


	void Initialise()
	{
		HeapChunk::InitialiseChunks();
	}






namespace HeapChunk
{
	void InitialiseChunks()
	{
		// Starting heap size is one page.
		HAL::VMem::MapAddr(HeapAddress, HAL::PMem::AllocatePage_NoMap(), 0x07);
		Memory::Set64((void*)HeapAddress, 0x00, 0x1000 / 8);

		// Setup one chunk

		// Header first.
		HeapHeader_type *Header		= (HeapHeader_type*)HeapAddress;
		Header->Flags				= 0x00;
		Header->IsFree				= true;
		Header->Magic				= ChunkMagic;
		Header->Size				= 0x1000 - sizeof(HeapHeader_type);
		Header->PreviousHeader		= Header;


		// Update the chunk counter
		ChunksInHeap = 1;
		TopOfHeap = HeapAddress + 0x1000;
	}

	uint64_t FindFirstFreeSlot(uint64_t Size)
	{
		uint64_t lookup = 0 + HeapAddress;
		HeapHeader_type* Header = (HeapHeader_type*)lookup;

		while(lookup < TopOfHeap - sizeof(HeapHeader_type))
		{
			// sanity check
			assert(Header->Magic == ChunkMagic, "Header's magic value corrupted! Check header alignment.");

			// if it's free, return it.
			if(Header->IsFree && Header->Size >= Size)
				return (uint64_t)Header;

			lookup += (sizeof(HeapHeader_type) + Header->Size);
			Header = (HeapHeader_type*)lookup;
		}
		return -1;
	}








	uint64_t ExpandHeap(uint64_t PagesToAdd)
	{
		if(PagesToAdd + SizeOfHeapInPages > MaximumHeapSizeInPages)
		{
			HALT("Out of memory! (heap)");
		}

		// allocate and map.
		for(uint64_t k = 0; k < PagesToAdd; k++)
		{
			HAL::VMem::MapAddr(TopOfHeap + (k * 0x1000), HAL::AllocatePage(), 0x07);
		}



		// Loop from the top of the heap.
		uint64_t z = TopOfHeap - sizeof(HeapHeader_type);

		HeapHeader_type* Header = (HeapHeader_type*)z;

		while(Header->Magic != ChunkMagic)
		{
			z--;
			Header = (HeapHeader_type*)z;
		}

		assert(Header->Magic == ChunkMagic, "Header's magic value corrupted! Check header alignment.");
		assert((uint64_t)Header + sizeof(HeapHeader_type) + Header->Size == TopOfHeap, "Failed to find last chunk!");


		// now... if the chunk is free, our lives are simple.
		if(Header->IsFree)
		{
			Header->Size += (PagesToAdd * 0x1000);
			TopOfHeap += (PagesToAdd * 0x1000);
			SizeOfHeapInPages += PagesToAdd;

			return SizeOfHeapInPages;
		}
		else
		{
			// now the job isn't that simple.
			// we need to create a new chunk after this one.

			// first, memset the area we need.
			Memory::Set((void*)TopOfHeap, 0x00, sizeof(HeapHeader_type));

			// next, setup the fields.
			HeapHeader_type* Header2 = (HeapHeader_type*)TopOfHeap;

			Header2->Flags			= 0x00;
			Header2->IsFree			= true;
			Header2->Magic			= ChunkMagic;
			Header2->Size			= (PagesToAdd * 0x1000) - sizeof(HeapHeader_type);
			Header2->PreviousHeader	= Header;


			assert(Header2->Magic == ChunkMagic, "Header's magic value corrupted! Check header alignment.");

			// finalise the change.
			TopOfHeap += (PagesToAdd * 0x1000);
			SizeOfHeapInPages += PagesToAdd;
			ChunksInHeap++;

			assert((uint64_t)Header2 + sizeof(HeapHeader_type) + Header2->Size == TopOfHeap, "Header was masquerading as last header, but actually isn't!");



			assert(((HeapHeader_type*)HeapAddress)->Magic == ChunkMagic, "Header's magic value corrupted! Check header alignment.");
			((HeapHeader_type*)HeapAddress)->PreviousHeader = Header2;

			return SizeOfHeapInPages;
		}
	}



	uint64_t SplitChunk(HeapHeader_type* Header, uint64_t Size)
	{
		// Make sure Addr contains the address of the *HEADER*
		// Splits a chunk into two;
		// the Size parameter is the desired size of the first chunk;
		// the returned address is the address (of the header) of the second chunk.

		// compulsory sanity checks.
		assert(Header->Magic == ChunkMagic, "Header's magic value corrupted! Check header alignment.");
		assert(Header->Size >= Size, "Invalid size to HeapChunk::SplitChunk(), Check your call chain.");
		assert(Header->IsFree, "Tried to split chunk in use!");
		if(Header->Size == Size)
			return 0;


		uint64_t OldSize = Header->Size;



		if(OldSize - Size - sizeof(HeapHeader_type) > 0 &&
			(uint64_t)Header + sizeof(HeapHeader_type) + Size + sizeof(HeapHeader_type) < TopOfHeap)
		{
			// we can make a new chunk here, no problems.
			HeapHeader_type* Header2 = (HeapHeader_type*)((uint64_t)Header + sizeof(HeapHeader_type) + Size);

			Memory::Set((void*)Header2, 0x00, sizeof(HeapHeader_type));


			Header2->Flags			= 0x00;
			Header2->IsFree			= true;
			Header2->Magic			= ChunkMagic;
			Header2->Size			= OldSize - Size - sizeof(HeapHeader_type);
			Header2->PreviousHeader	= Header;

			assert(Header2->Magic == ChunkMagic, "Header's magic value corrupted! Check header alignment.");

			// Finalise the size change operation.
			Header->Size = Size;
			ChunksInHeap++;



			if((uint64_t)Header2 + sizeof(HeapHeader_type) + Header2->Size == TopOfHeap)
			{
				assert(((HeapHeader_type*)HeapAddress)->Magic == ChunkMagic, "Header's magic value corrupted! Check header alignment.");
				((HeapHeader_type*)HeapAddress)->PreviousHeader = Header2;
			}

			return (uint64_t)Header2;
		}
		else
		{
			// we can't make a new chunk in this space.
			// leave the chunk as it is, extra space can't hurt too much.
			// DBG_PrintChunks();
			return 0;
		}
	}


	uint64_t ContractHeap(uint64_t PagesToContract)
	{
		return SizeOfHeapInPages;
	}

	uint64_t GetHeapSizeInPages()
	{
		return SizeOfHeapInPages;
	}



	uint64_t QuerySize(void* Address)
	{
		HeapHeader_type* Header = (HeapHeader_type*)((uint8_t*)Address - sizeof(HeapHeader_type));
		if(Header->Magic != ChunkMagic)
			return 0;

		else
			return Header->Size;
	}








	HeapHeader_type* GetFirstHeader()
	{
		return (HeapHeader_type*)HeapAddress;
	}


	void DBG_PrintChunks()
	{
		// TODO: Prettify output
		uint64_t i = HeapAddress;
		uint64_t z = 0;
		while(z < ChunksInHeap)
		{
			HeapHeader_type* Header = (HeapHeader_type*)i;

			assert(Header->Magic == ChunkMagic, "Header's magic value corrupted! Check header alignment.");

			// don't use assert, we need to if-else to break the loop
			printk("%s at: %x, Size: %x bytes; Data at: %x\n", Header->Flags & 1 ? "Hole" : "Chunk", i - HeapAddress, Header->Size, (uint64_t)Header + sizeof(HeapHeader_type)  - HeapAddress);

			i += sizeof(HeapHeader_type) + Header->Size;
			z++;

			if(i == TopOfHeap)
				break;
		}
		printk("----------------------\n\n");
	}









}
}
}
}




