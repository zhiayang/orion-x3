// vgatext.c
// Property of Aperture Research Inc (c) 2012-2013
// Licensed under Creative Commons Attribution 3.0 Unported.

#include <stdlib.h>

using namespace Kernel;
using namespace Library;
using namespace Kernel::HAL::Devices::LFB;





int VT_CursorX = 0;
int VT_CursorY = 0;

int CharsPerLine;
int CharsPerPage;
int CharsPerColumn;

uint32_t VT_Colour = 0xFFFFFFFF;
bool VT_DidInit = false;


namespace Kernel {
namespace HAL {
namespace Devices {
namespace LFB {
namespace Console
{
	Library::Types::Queue* CharBuffer;

	void Initialise()
	{
		CharsPerLine = GetResX() / 8 - 1;
		CharsPerPage = CharsPerLine * (GetResY() / 16) - 1;
		CharsPerColumn = CharsPerPage / CharsPerLine;

		VT_DidInit = true;
	}

	bool IsInitialised()
	{
		return VT_DidInit;
	}

	char PrintChar(char c)
	{
		if(c == 0)
			return c;

		HAL::Devices::COM1::WriteChar(c);

		if(!IsInitialised())
			return 0;

		if(c == '\r')
		{
			VT_CursorX = 0;
			return c;
		}

		if(c == '\b')
		{
			if(VT_CursorX > 0)
			{
				VT_CursorX--;
				DrawChar(' ', (VT_CursorX * 8), (VT_CursorY * 16), VT_Colour);
			}
			else if(VT_CursorY > 0)
			{
				VT_CursorX = CharsPerLine - 1;
				VT_CursorY--;

				DrawChar(' ', (VT_CursorX * 8), (VT_CursorY * 16), VT_Colour);
			}
			return c;
		}

		if(VT_CursorY == CharsPerColumn && VT_CursorX == CharsPerLine)
		{
			// Reached end of line and no more space below
			VT_CursorX = 0;
			ScrollDown(1);



			if(c == '\n' || c == '\t')
			{
				ScrollDown(1);
				VT_CursorX = 0;
				return c;
			}
			DrawChar(c, (VT_CursorX * 8), (VT_CursorY * 16), VT_Colour);
			VT_CursorX++;

		}
		else if((VT_CursorX * 8) >= ((GetResX()) - 10))
		{
			// Reached end of line
			VT_CursorX = 0;
			VT_CursorY++;

			DrawChar(c, (VT_CursorX * 8), (VT_CursorY * 16), VT_Colour);
			VT_CursorX = 1;
		}
		else
		{
			if(c == '\n')
			{
				if(VT_CursorY < CharsPerColumn)
				{
					VT_CursorX = 0;
					VT_CursorY++;
					return c;
				}
				else
				{
					VT_CursorX = 0;
					ScrollDown(1);
					return c;
				}
			}
			else if(c == '\t')
			{
				if(((VT_CursorX + 4) & ~(4 - 1)) < CharsPerLine)
				{
					VT_CursorX = (VT_CursorX + 4) & ~(4 - 1);
				}
				else
				{
					VT_CursorX = 0;
					if(VT_CursorY < CharsPerColumn)
						VT_CursorY++;

					else
						ScrollDown(1);
				}
				return c;
			}

			// Normal printing
			DrawChar(c, (VT_CursorX * 8), (VT_CursorY * 16), VT_Colour);
			VT_CursorX++;
		}
		return c;
	}

	void ClearScreen()
	{
		Memory::Set((uint64_t*)Internal::GetLFBAddr(), 0x00, GetResX() * GetResY() * 4);
		Memory::Set((uint64_t*)Internal::_GetTrueLFBAddr(), 0x00, GetResX() * GetResY() * 4);
		VT_CursorX = 0;
		VT_CursorY = 0;
	}

	void Scroll()
	{
		// This is a sensitive operation, don't interrupt us while we scroll
		// asm volatile("cli");
		Memory::CopyOverlap((uint64_t*)Internal::GetLFBAddr(), (uint64_t*)Internal::GetLFBAddr() + (GetResX() * 8), (GetResX() * GetResY() * 3.9));
		Memory::Set((uint64_t*)(Internal::GetLFBAddr() + (GetResX() * (GetResY() - 16)) * 4), 0x00, GetResX() * 16 * 4);
		// asm volatile("sti");
	}

	void ScrollDown(int lines)
	{
		while(lines > 0)
		{
			Scroll();
			lines--;
		}
		return;
	}


	void Backspace(int characters)
	{
	}

	void SetColour(uint32_t Colour)
	{
		VT_Colour = Colour;
	}


	void MoveCursor(int x, int y)
	{
		if(x < CharsPerLine && y < CharsPerColumn)
		{
			VT_CursorX = x;
			VT_CursorY = y;
		}
	}

	int GetCharsPerLine()
	{
		return CharsPerLine;
	}

	int GetCharsPerColumn()
	{
		return CharsPerColumn;
	}

	int GetCharsPerPage()
	{
		return CharsPerPage;
	}

	int GetCursorX()
	{
		return VT_CursorX;
	}
	int GetCursorY()
	{
		return VT_CursorY;
	}
}
}
}
}
}


