// FAT32.cpp
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.


#include <namespace.h>
#include <stdlib.h>

using namespace Library::Stdio;
using namespace Library;

namespace Kernel {
namespace HAL {
namespace FS
{
	FAT32::FAT32(Partition* ParentP) : Filesystem(ParentP, fat32)
	{
		using namespace Devices::ATA::PIO;
		using Devices::ATA::ATADevice;

		// this->ParentPartition = ParentP;
		this->partition = ParentP;

		// printk("{%x} {%x}\n", this->Parent, Parent);
		this->Type = fat32;

		// read the fields from LBA 0
		ATADevice* atadev = ParentP->GetDrive();

		// make sure the FAT32 partition signature is there -- essentially just a MBR signature.
		if(atadev->Data[255] != 0xAA55)
			printk("\t\t\t- Invalid FAT32 signature! Expected [%x], got [%x] instead -- Check your Partition.\n", 0xAA55, atadev->Data[255]);


		atadev->ReadSector(ParentP->GetStartLBA());
		uint8_t* fat = (uint8_t*)atadev->Data;





		this->BytesPerSector = (*((uint16_t*)(fat + 11)));
		this->SectorsPerCluster = *(fat + 13);
		this->ReservedSectors = (*((uint16_t*)(fat + 14)));
		this->NumberOfFATs = *(fat + 16);
		this->NumberOfDirectories = (*((uint16_t*)(fat + 17)));

		((uint16_t)(*((uint16_t*)(fat + 17))) > 0) ? this->TotalSectors = (uint16_t)(*((uint16_t*)(fat + 17))) : this->TotalSectors = (uint32_t)(*((uint32_t*)(fat + 32)));

		this->HiddenSectors = (*((uint32_t*)(fat + 28)));

		this->FATSectorSize = (*((uint32_t*)(fat + 36)));
		this->RootDirectoryCluster = (uint32_t)(*((uint32_t*)(fat + 44)));
		this->FSInfoCluster = (*((uint16_t*)(fat + 48)));

		this->BackupBootCluster = (*((uint16_t*)(fat + 50)));

		this->FirstUsableCluster = this->partition->GetStartLBA() + this->ReservedSectors + (this->NumberOfFATs * this->FATSectorSize);


	}

	void FAT32::ListFiles()
	{
		SearchFile(2, "", true, true);
	}

	FAT32::File_type* FAT32::SearchFile(uint32_t Cluster, const char* name, bool Recursive, bool Verbose)
	{
		char* n2 = (char*)HAL::DMem::AllocateChunk(16);
		Devices::ATA::PIO::ReadSector(this->ParentPartition->GetDrive(), this->FirstUsableCluster + Cluster * this->SectorsPerCluster - (2 * this->SectorsPerCluster));
		uint8_t* file = (uint8_t*)this->ParentPartition->GetDrive()->Data;
		uint64_t lastsector = this->FirstUsableCluster + Cluster * this->SectorsPerCluster - (2 * this->SectorsPerCluster);

		static uint32_t tablevel = 1;


		for(int z = 0; z < (this->SectorsPerCluster * 512 / 32); file += 32, z++)
		{
			Devices::ATA::PIO::ReadSector(this->ParentPartition->GetDrive(), this->FirstUsableCluster + (z * 32 / 512) + Cluster * this->SectorsPerCluster - (2 * this->SectorsPerCluster));


			if(lastsector != this->FirstUsableCluster + (z * 32 / 512) + Cluster * this->SectorsPerCluster - (2 * this->SectorsPerCluster))
			{
				file = (uint8_t*)this->ParentPartition->GetDrive()->Data;
				lastsector = this->FirstUsableCluster + (z * 32 / 512) + Cluster * this->SectorsPerCluster - (2 * this->SectorsPerCluster);
			}

			if(*file != 0xE5 && *file != 0x2E && *file != 0x05 && *file != 0x00 && !(*(file + 11) & (1 << 1)) && !(*(file + 11) & (1 << 2)) && !(*(file + 11) & (1 << 3)) && !(*(file + 11) & 0x0F))
			{
				n2[0] = *(file + 0);
				n2[1] = *(file + 1);
				n2[2] = *(file + 2);
				n2[3] = *(file + 3);
				n2[4] = *(file + 4);
				n2[5] = *(file + 5);
				n2[6] = *(file + 6);
				n2[7] = *(file + 7);
				n2[8] = *(file + 8);
				n2[9] = *(file + 9);
				n2[10] = *(file + 10);
				n2[11] = 0;
				uint32_t size = *((uint32_t*)(file + 28));


				if(*(file + 11) & (1 << 4))
				{
					if(Verbose)
					{
						for(uint32_t t = 0; t < tablevel; t++)
							printk("\t");

						printk("Folder: [%s]\n", GetFolderName(n2));
					}

					if(Recursive)
					{
						tablevel++;
						uint32_t cr = *((uint16_t*)(file + 26)) | *((uint16_t*)(file + 20)) << 8;
						FAT32::File_type* result = SearchFile(cr, name, true, Verbose);
						if(result)
							return result;

						tablevel--;
					}
				}
				else
				{
					if(Verbose)
					{
						for(uint32_t t = 0; t < tablevel; t++)
							printk("\t");

						printk("File: [%s] -> %d bytes\n", GetFileName(n2), size);
					}

					if(String::Compare(GetFileName(n2), name))
					{
						// we've found the file: return the absolute byte offset from partition's beginning.
						// TODO: implement more robust way to return this value.
						// This will probably work for the forseeable lifespan of this OS though, until 16EB (2^64 - 1) byte drives
						// are manufactured.


						FAT32::File_type* x = (FAT32::File_type*)HAL::DMem::AllocateChunk(sizeof(File_type));

						String::Copy(x->Name, String::SubString(n2, 0, 8));
						String::Copy(x->Extension, String::SubString(n2, 8, 3));
						x->RawAttrib = *(file + 0xB);
						x->UserAttributes = *(file + 0xC);
						x->CreationTimeMiliseconds = *(file + 0xD);
						x->RawCTime = *((uint16_t*)(file + 0xE));
						x->RawCDate = *((uint16_t*)(file + 0x10));
						x->Unused = *((uint16_t*)(file + 0x12));
						x->HighCluster = *((uint16_t*)(file + 0x14));
						x->RawMTime = *((uint16_t*)(file + 0x16));
						x->RawCDate = *((uint16_t*)(file + 0x18));
						x->LowCluster = *((uint16_t*)(file + 0x1A));
						x->FileSize = *((uint32_t*)(file + 0x1C));
						return (FAT32::File_type*)x;
					}
				}
			}
		}

		// the loop above is only engineered to read one cluster worth of files.
		// Dig into the FAT table; if there is another cluster in the chain, recursively call the function. again.


		Devices::ATA::PIO::ReadSector(this->ParentPartition->GetDrive(), this->FirstUsableCluster + Cluster * this->SectorsPerCluster - (2 * this->SectorsPerCluster));
		file = (uint8_t*)this->ParentPartition->GetDrive()->Data;



		uint32_t c3 = 0;

		Devices::ATA::PIO::ReadSector(this->ParentPartition->GetDrive(), this->ParentPartition->GetStartLBA() + this->ReservedSectors + (Cluster * 4 / 512));
		uint8_t* fat2 = (uint8_t*)this->ParentPartition->GetDrive()->Data;
		uint32_t offset = ((Cluster * 4) % 512);
		c3 = *((uint32_t*)(fat2 + offset));

		if((c3 != 0) && ((c3 & 0x0FFFFFFF) < 0x0FFFFFF8))
		{
			File_type* result = SearchFile(c3, name, Recursive, Verbose);
			if(result)
				return result;
		}

		return 0;
	}

	char* FAT32::GetFileName(char* filename)
	{
		if(String::Length(filename) == 11)
		{
			// if this is a short filename...

			char* ext = (char*)HAL::DMem::AllocateChunk(16);


			ext[0] = filename[8];
			ext[1] = filename[9];
			ext[2] = filename[10];
			ext[3] = filename[11];

			// String::Copy(ext, filename);
			filename = String::SubString(filename, 0, 8);
			// ext = String::SubString(ext, 8, 3);

			String::TrimWhitespace(filename);
			String::TrimWhitespace(ext);
			if(String::Compare(ext, "   "))
			{
				// HAL::DMem::FreeChunk((uint64_t*)ext);
				return filename;
			}
			else
			{
				String::ConcatenateChar(filename, '.');
				String::Concatenate(filename, ext);

				// HAL::DMem::FreeChunk((uint64_t*)ext);
				return filename;
			}
		}
		return (char*)"";
	}

	char* FAT32::GetFolderName(char* foldername)
	{
		return String::TrimWhitespace(foldername);
	}

	void FAT32::ReadFile(File_type* File, uint64_t Address)
	{
		// make sure the caller gave a sane address.


		uint64_t ReadUntil = HAL::DMem::HeapChunk::QuerySize((void*)Address);

		uint64_t BufferOffset = 0;
		using namespace HAL::Devices::ATA::PIO;
		uint32_t Cluster = File->LowCluster | (File->HighCluster << 16);
		uint32_t cchain = 0;

		// read the cluster chain
		do
		{
			uint32_t FatSector = this->ParentPartition->GetStartLBA() + this->ReservedSectors + (Cluster * 4 / 512);
			uint32_t FatOffset = (Cluster * 4) % 512;

			ReadSector(this->ParentPartition->GetDrive(), FatSector);
			uint8_t* clusterchain = (uint8_t*)this->ParentPartition->GetDrive()->Data;
			cchain = *((uint32_t*)&clusterchain[FatOffset]) & 0x0FFFFFFF;

			// cchain is the next cluster in the list.

			// but read this cluster's first sector.
			// we need to handle cases where the cluster size is larger than the sector size, and the file is larger than 512 bytes.

			for(uint16_t co = 0; co < (File->FileSize < 512 ? 1 : (File->FileSize / 512 < this->SectorsPerCluster ? File->FileSize : this->SectorsPerCluster)); co++)
			{
				ReadSector(this->ParentPartition->GetDrive(), this->FirstUsableCluster + co + Cluster * this->SectorsPerCluster - (2 * this->SectorsPerCluster));

				uint8_t* contents = (uint8_t*)this->ParentPartition->GetDrive()->Data;

				for(int i = 0; i < 512 && i < ReadUntil; i++)
				{
					*((uint8_t*)((uint8_t*)Address + BufferOffset)) = contents[i];
					BufferOffset++;
				}
				ReadUntil -= 0x200;
				// printk("<%x, %x>\n", ((uint8_t*)((uint8_t*)Address + BufferOffset)), Address + ReadUntil);
			}
			Cluster = cchain;

		} while((cchain != 0) && !((cchain & 0x0FFFFFFF) >= 0x0FFFFFF8));

	}


	uint16_t	FAT32::GetBytesPerSector(){ return this->BytesPerSector; }
	uint8_t		FAT32::GetSectorsPerCluster(){ return this->SectorsPerCluster; }
	uint16_t	FAT32::GetReservedSectors(){ return this->ReservedSectors; }
	uint8_t		FAT32::GetNumberOfFATS(){ return this->NumberOfFATs; }
	uint16_t	FAT32::GetNumberOfDirectories(){ return this->NumberOfDirectories; }

	uint32_t	FAT32::GetTotalSectors(){ return this->TotalSectors; }
	uint32_t	FAT32::GetHiddenSectors(){ return this->HiddenSectors; }
	uint32_t	FAT32::GetFATSectorSize(){ return this->FATSectorSize; }
	uint32_t	FAT32::GetRootDirectoryCluster(){ return this->RootDirectoryCluster; }

	uint16_t	FAT32::GetFSInfoCluster(){ return this->FSInfoCluster; }
	uint16_t	FAT32::GetBackupBootCluster(){ return this->BackupBootCluster; }
	uint32_t	FAT32::GetFirstUsableCluster(){ return this->FirstUsableCluster; }










}
}
}
