// HFS+.cpp
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.

#include <namespace.h>
#include <stdlib.h>

using namespace Library::Stdio;
using namespace Kernel::HAL::Devices::ATA;

namespace Kernel {
namespace HAL {
namespace FS
{
	// HFSPlus::HFSPlus(Partition* parent) : Filesystem(parent, hfsplus)
	// {
	// 	// the most annoying thing about HFS+ is that it was designed when Mac OS/OS X was still using PPC
	// 	// processors... which are big endian.

	// 	// return;

	// 	// read the 3rd sector -- HFS+ volume header is stored at 1024 bytes

	// 	using namespace HAL::Device::ATA::PIO;

	// 	PIO::ReadSector((ATADevice*)((*ATADevice::ATADevices)[1]), 2);
	// 	PIO::ReadSector((ATADevice*)((*ATADevice::ATADevices)[1]), 42);
	// }
}
}
}
