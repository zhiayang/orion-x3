// bga.c
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.


// Implements graphics card interfacing functions, using standard VBE with the Bochs/Qemu emulated card.


#include <stdlib.h>


namespace Kernel {
namespace HAL {
namespace Devices {
namespace BGA
{

	void WriteRegister(unsigned short IndexValue, uint16_t DataValue)
	{
		outw(VBE_DISPI_IOPORT_INDEX, IndexValue);
		outw(VBE_DISPI_IOPORT_DATA, DataValue);
	}

	uint16_t ReadRegister(unsigned short IndexValue)
	{
		outw(VBE_DISPI_IOPORT_INDEX, IndexValue);
		return inw(VBE_DISPI_IOPORT_DATA);
	}

	uint8_t IsAvailable(void)
	{
		return (ReadRegister(VBE_DISPI_INDEX_ID) == 0xB0C5);
	}

	void SetVideoMode(unsigned int Width, unsigned int Height, unsigned int BitDepth, uint8_t ClearVideoMemory, uint8_t GetCaps)
	{
		WriteRegister(VBE_DISPI_INDEX_ENABLE, VBE_DISPI_DISABLED);
		WriteRegister(VBE_DISPI_INDEX_XRES, Width);
		WriteRegister(VBE_DISPI_INDEX_YRES, Height);
		WriteRegister(VBE_DISPI_INDEX_BPP, BitDepth);
		WriteRegister(VBE_DISPI_INDEX_ENABLE, VBE_DISPI_LFB_ENABLED |
	        (ClearVideoMemory ? 1 : ~VBE_DISPI_NOCLEARMEM) | (GetCaps ? VBE_DISPI_GETCAPS : 0));
	}


	uint16_t GetResX()
	{
		return ReadRegister(VBE_DISPI_INDEX_XRES);
	}

	uint16_t GetResY()
	{
		return ReadRegister(VBE_DISPI_INDEX_YRES);
	}

	void Disable()
	{
		WriteRegister(VBE_DISPI_INDEX_ENABLE, VBE_DISPI_DISABLED);
	}
}
}
}
}

















