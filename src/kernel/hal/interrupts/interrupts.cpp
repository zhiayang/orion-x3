// Interrupts.c
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.


// Does good things about interrupt handling.

#include <stdlib.h>

using namespace Kernel;
using namespace Library;
using namespace Library::Stdio;

namespace Kernel {
namespace HAL {
namespace Interrupts
{

	static const char *ExceptionMessages[] =
	{
		"Division By Zero",
		"Debug",
		"Non Maskable Interrupt",
		"Breakpoint",
		"Into Detected Overflow",
		"Out of Bounds",
		"Invalid Opcode",
		"No Coprocessor",

		"Double Fault",
		"Coprocessor Segment Overrun",
		"Bad TSS",
		"Segment Not Present",
		"Stack Fault",
		"General Protection Fault",
		"Page Fault",
		"Reserved",

		"x87 FPU Fault",
		"Alignment Check",
		"Machine Check",
		"SSE Fault",
		"Reserved",
		"Reserved",
		"Reserved",
		"Reserved",

		"Reserved",
		"Reserved",
		"Reserved",
		"Reserved",
		"Reserved",
		"Reserved",
		"Reserved",
		"Reserved"
	};




	extern "C" void isr0();
	extern "C" void isr1();
	extern "C" void isr2();
	extern "C" void isr3();
	extern "C" void isr4();
	extern "C" void isr5();
	extern "C" void isr6();
	extern "C" void isr7();
	extern "C" void isr8();
	extern "C" void isr9();
	extern "C" void isr10();
	extern "C" void isr11();
	extern "C" void isr12();
	extern "C" void isr13();
	extern "C" void isr14();
	extern "C" void isr15();
	extern "C" void isr16();
	extern "C" void isr17();
	extern "C" void isr18();
	extern "C" void isr19();
	extern "C" void isr20();
	extern "C" void isr21();
	extern "C" void isr22();
	extern "C" void isr23();
	extern "C" void isr24();
	extern "C" void isr25();
	extern "C" void isr26();
	extern "C" void isr27();
	extern "C" void isr28();
	extern "C" void isr29();
	extern "C" void isr30();
	extern "C" void isr31();

	extern "C" void irq0();
	extern "C" void irq1();
	extern "C" void irq2();
	extern "C" void irq3();
	extern "C" void irq4();
	extern "C" void irq5();
	extern "C" void irq6();
	extern "C" void irq7();
	extern "C" void irq8();
	extern "C" void irq9();
	extern "C" void irq10();
	extern "C" void irq11();
	extern "C" void irq12();
	extern "C" void irq13();
	extern "C" void irq14();
	extern "C" void irq15();

	extern "C" void HAL_AsmLoadIDT(uint64_t);

	struct __attribute__((packed)) IDTEntry
	{
		uint16_t base_lo;
		uint16_t sel;
		uint8_t always0_ist;
		uint8_t flags;
		uint16_t base_mid;
		uint32_t base_hi;
		uint32_t always0_1;
	};

	struct __attribute__((packed)) IDTPointer
	{
		uint16_t limit;
		uint64_t base;
	};








	static IDTEntry idt[256];
	static IDTPointer idtp;

	// Use this function to set an entry in the IDT. Alot simpler
	// than twiddling with the GDT ;)
	void SetGate(uint8_t num, uint64_t base, uint16_t sel, uint8_t flags)
	{
		// The interrupt routine's base address
		idt[num].base_lo = (base & 0xFFFF);
		idt[num].base_mid = (base >> 16) & 0xFFFF;
		idt[num].base_hi = (base >> 32) & 0xFFFFFFFF;

		// The segment or 'selector' that this IDT entry will use
		// is set here, along with any access flags

		idt[num].sel = sel;
		if(num < 32)
			idt[num].always0_ist = 0x0;

		else
			idt[num].always0_ist = 0x0;

		idt[num].always0_1 = 0;
		idt[num].flags = flags;
	}

	// Installs the IDT
	void Initialise()
	{

		// Sets the special IDT pointer up, just like in 'gdt.c'
		idtp.limit = (sizeof(IDTEntry) * 256) - 1;
		idtp.base = (uintptr_t)&idt;

		// Clear out the entire IDT, initializing it to zeros
		Memory::Set(&idt, 0, sizeof(IDTEntry) * 256);

		// Add any new ISRs to the IDT here using idt_set_gate

		InstallDefaultHandlers();


		// Points the processor's internal register to the new IDT
		HAL_AsmLoadIDT((uint64_t)&idtp);
	}

	void InstallDefaultHandlers()
	{
		SetGate(0, (uint64_t)isr0, 0x08, 0x8E);
		SetGate(1, (uint64_t)isr1, 0x08, 0x8E);
		SetGate(2, (uint64_t)isr2, 0x08, 0x8E);
		SetGate(3, (uint64_t)isr3, 0x08, 0x8E);
		SetGate(4, (uint64_t)isr4, 0x08, 0x8E);
		SetGate(5, (uint64_t)isr5, 0x08, 0x8E);
		SetGate(6, (uint64_t)isr6, 0x08, 0x8E);
		SetGate(7, (uint64_t)isr7, 0x08, 0x8E);

		SetGate(8, (uint64_t)isr8, 0x08, 0x8E);
		SetGate(9, (uint64_t)isr9, 0x08, 0x8E);
		SetGate(10, (uint64_t)isr10, 0x08, 0x8E);
		SetGate(11, (uint64_t)isr11, 0x08, 0x8E);
		SetGate(12, (uint64_t)isr12, 0x08, 0x8E);
		SetGate(13, (uint64_t)isr13, 0x08, 0x8E);
		SetGate(14, (uint64_t)isr14, 0x08, 0x8E);
		SetGate(15, (uint64_t)isr15, 0x08, 0x8E);

		SetGate(16, (uint64_t)isr16, 0x08, 0x8E);
		SetGate(17, (uint64_t)isr17, 0x08, 0x8E);
		SetGate(18, (uint64_t)isr18, 0x08, 0x8E);
		SetGate(19, (uint64_t)isr19, 0x08, 0x8E);
		SetGate(20, (uint64_t)isr20, 0x08, 0x8E);
		SetGate(21, (uint64_t)isr21, 0x08, 0x8E);
		SetGate(22, (uint64_t)isr22, 0x08, 0x8E);
		SetGate(23, (uint64_t)isr23, 0x08, 0x8E);

		SetGate(24, (uint64_t)isr24, 0x08, 0x8E);
		SetGate(25, (uint64_t)isr25, 0x08, 0x8E);
		SetGate(26, (uint64_t)isr26, 0x08, 0x8E);
		SetGate(27, (uint64_t)isr27, 0x08, 0x8E);
		SetGate(28, (uint64_t)isr28, 0x08, 0x8E);
		SetGate(29, (uint64_t)isr29, 0x08, 0x8E);
		SetGate(30, (uint64_t)isr30, 0x08, 0x8E);
		SetGate(31, (uint64_t)isr31, 0x08, 0x8E);


		// Remap the IRQs from 0 - 7 -> 8 - 15 to 32-47
		outb(0x20, 0x11);
		outb(0xA0, 0x11);
		outb(0x21, 0x20);
		outb(0xA1, 0x28);
		outb(0x21, 0x04);
		outb(0xA1, 0x02);
		outb(0x21, 0x01);
		outb(0xA1, 0x01);
		outb(0x21, 0x0);
		outb(0xA1, 0x0);

		SetGate(32, (uint64_t)irq0, 0x08, 0x8E);
		SetGate(33, (uint64_t)irq1, 0x08, 0x8E);
		SetGate(34, (uint64_t)irq2, 0x08, 0x8E);
		SetGate(35, (uint64_t)irq3, 0x08, 0x8E);
		SetGate(36, (uint64_t)irq4, 0x08, 0x8E);
		SetGate(37, (uint64_t)irq5, 0x08, 0x8E);
		SetGate(38, (uint64_t)irq6, 0x08, 0x8E);
		SetGate(39, (uint64_t)irq7, 0x08, 0x8E);
		SetGate(40, (uint64_t)irq8, 0x08, 0x8E);
		SetGate(41, (uint64_t)irq9, 0x08, 0x8E);
		SetGate(42, (uint64_t)irq10, 0x08, 0x8E);
		SetGate(43, (uint64_t)irq11, 0x08, 0x8E);
		SetGate(44, (uint64_t)irq12, 0x08, 0x8E);
		SetGate(45, (uint64_t)irq13, 0x08, 0x8E);
		SetGate(46, (uint64_t)irq14, 0x08, 0x8E);
		SetGate(47, (uint64_t)irq15, 0x08, 0x8E);
	}




	static void *IRQHandlerMap[16] =
	{
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0
	};

	/* This installs a custom IRQ handler for the given IRQ */
	void InstallIRQHandler(int irq, void (*Handler)(HAL_ISRRegs *r))
	{
		IRQHandlerMap[irq] = (void*)Handler;
	}
	void UninstallIRQHandler(int irq)
	{
		IRQHandlerMap[irq] = 0;
	}











	void PrintRegisterDump(HAL_ISRRegs *r)
	{
		printk("%wRegisters:\n", VC_Blue);


		printk("rax:\t%w%16x%r\trbx:\t%w%16x\n", VC_Green, r->rax, VC_Green, r->rbx);
		printk("rcx:\t%w%16x%r\trdx:\t%w%16x\n", VC_Green, r->rcx, VC_Green, r->rdx);
		printk("r08:\t%w%16x%r\tr09:\t%w%16x\n", VC_Green, r->r8, VC_Green, r->r9);
		printk("r10:\t%w%16x%r\tr11:\t%w%16x\n", VC_Green, r->r10, VC_Green, r->r11);
		printk("r12:\t%w%16x%r\tr13:\t%w%16x\n", VC_Green, r->r12, VC_Green, r->r13);
		printk("r14:\t%w%16x%r\tr15:\t%w%16x\n", VC_Green, r->r14, VC_Green, r->r15);

		printk("rdi:\t%w%16x%r\trsi:\t%w%16x\n", VC_Cyan, r->rdi, VC_Cyan, r->rsi);

		printk("rbp:\t%w%16x%r\trsp:\t%w%16x\n", VC_Yellow, r->rbp, VC_Yellow, r->rsp);

		printk("rip:\t%w%16x%r\tcs: \t%w%16x\n", VC_Cyan, r->rip, VC_Silver, r->cs);

		printk("ss: \t%w%16x%r\tu-rsp:\t%w%16x\n", VC_Silver, r->ss, VC_Blue, r->useresp);

		printk("rflags:\t%w%16x%r\tcr2:\t%w%16x\n", VC_Magenta, r->rflags, VC_Red, r->cr2);
	}






	extern "C" void HAL_ISRHandler(HAL_ISRRegs *r)
	{
		uint64_t iid = r->InterruptID;
		if(iid >= 32)
		{
			// Send EOIs (IRQ ACK)



			if(IRQHandlerMap[iid - 32])
			{
				void (*Handler)(HAL_ISRRegs *r) = (void (*)(HAL_ISRRegs *r))IRQHandlerMap[iid - 32];
				Handler(r);
			}


			// This IRQ is >7, send an EOI to the slave controller too.
			if(iid >= 40)
			{
				outb(0xA0, 0x20);
			}

			// We need to send the ACK to the master regardless.
			outb(0x20, 0x20);

			return;
		}







		printk("\n\n\n%wCPU Exception: %w%s%w; Error Code: %w%x", VC_Yellow, VC_Red, ExceptionMessages[r->InterruptID], VC_Yellow, VC_Red, r->ErrorCode);



		printk("\nOrion-X3 has met an unresolvable error, and will now halt.\n");


		printk("Debug Information:\n\n");
		if(r->InterruptID == 14)
		{
			uint64_t cr2;
			asm volatile("mov %%cr2, %0" : "=r" (cr2));

			// The error code gives us details of what happened.
			uint8_t PageNotPresent			= !(r->ErrorCode & 0x1);	// Page not present
			uint8_t PageAccess				= r->ErrorCode & 0x2;		// Write operation?
			uint8_t PageSupervisor			= r->ErrorCode & 0x4;		// Processor was in user-mode?
			uint8_t PageReservedBits		= r->ErrorCode & 0x8;		// Overwritten CPU-reserved bits of page entry?
			uint8_t PageInstructionFetch	= r->ErrorCode & 0x10;		// Caused by an instruction fetch?


			printk("%wPage Fault Error Codes:\n", VC_Green);

			printk("\tCR2: %x\n", cr2);



			if(PageNotPresent)
				printk("\tPage not present\n");

			if(PageAccess)
				printk("\tWriting to read-only page\n");

			if(PageSupervisor)
				printk("\tAccessing Supervisor page from User Mode\n");

			if(PageReservedBits)
				printk("\tOverwritten reserved bits\n");

			if(PageInstructionFetch)
				printk("\tPF Caused by instruction fetch\n");


			// if the address was above 0xFFFFFFFFA0000000, it's very likely it's a heap fault.
			// Print the chunk list, for review.
			if(cr2 >= HeapAddress)
			{
				printk("\n\n\n");
				// Kernel::HAL::DMem::HeapChunk::DBG_PrintChunks();
			}
		}

		printk("\n");

		// Kernel::HAL::DMem::HeapChunk::DBG_PrintChunks();
		if(!DEBUG || SKIPREGDMP)
			UHALT();


		PrintRegisterDump(r);


		UHALT();
	}
}

}
}
















