// Scheduler.c
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.

#include <stdlib.h>

using namespace Kernel;
using namespace Library;

bool IsFirst = true;
uint64_t CurrentPID = 0;
Multitasking::Task_type* TaskList;
extern "C" void DoUsermode(uint64_t, uint64_t);

uint64_t NumberOfTasks = 0;

namespace Kernel {
namespace Multitasking
{
	void Init()
	{
		TaskList = (Task_type*)HAL::DMem::AllocateChunk(sizeof(Task_type) * 20);
	}

	uint64_t GetNumberOfTasks()
	{
		return NumberOfTasks;
	}

	Multitasking::Task_type GetTask(uint64_t id)
	{
		return TaskList[id];
	}

	void SetTaskInList(uint64_t pid, Task_type* Task)
	{
		TaskList[pid] = *Task;
		NumberOfTasks++;
	}

	void Sleep(uint64_t Miliseconds)
	{
		// Stdio::printk("<<<%d>>>", CurrentPID);
		TaskList[CurrentPID].Sleep = Miliseconds;
	}
	void Yield()
	{
		asm volatile("int $32");
	}


	extern "C" uint64_t SwitchProcess(uint64_t context, uint64_t old_rip)
	{
		if(NumberOfTasks == 0)
			return context;


		// If this is the first task switch, %rsp (context) will actually be the main kernel stack.
		// Clearly we don't want this, so we skip this part for that.
		if(!IsFirst)
			TaskList[CurrentPID].StackPointer = context;      // save the old context into current task
		else
			IsFirst = false;


		// task switches should happen about 1000 times a second, or 1kHz.


		if(CurrentPID < NumberOfTasks - 1)
			CurrentPID++;

		else
			CurrentPID = 0;


		// Kernel::HAL::Devices::COM1::WriteString("\n<task: ");
		// Kernel::HAL::Devices::COM1::WriteString(TaskList[CurrentPID].Name);
		// Kernel::HAL::Devices::COM1::WriteString(", ");
		// Kernel::HAL::Devices::COM1::WriteString(Library::Utility::ConvertToString(TaskList[CurrentPID].StackPointer));
		// Kernel::HAL::Devices::COM1::WriteString(">");

		// Stdio::printk("[task: %s, %x]", TaskList[CurrentPID].Name, TaskList[CurrentPID].StackPointer);


		if(TaskList[CurrentPID].Flags & 0x1)
		{

			// this tells switch.s (on return) that we need to return to user-mode.
			asm volatile("mov $0xFADE, %%r11" ::: "%r11");
		}

		if(TaskList[CurrentPID].Sleep > 0)
		{
			TaskList[CurrentPID].Sleep--;
			CurrentPID = 1;
			return TaskList[1].StackPointer;
		}
		return TaskList[CurrentPID].StackPointer;			// Return new task's context.
	}

}
}




