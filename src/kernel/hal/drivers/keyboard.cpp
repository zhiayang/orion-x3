// Keyboard.cpp
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.

#include <namespace.h>
#include <stdlib.h>

using namespace Library::Stdio;

namespace Kernel {
namespace HAL {
namespace Drivers
{
	constexpr uint32_t PS2Keyboard::ScanCode2_US[2 * 132];
	constexpr uint32_t PS2Keyboard::ScanCode2_US_E0[2 * 128];
	constexpr uint32_t PS2Keyboard::ScanCode2_US_E0_F0[2 * 128];
	constexpr uint32_t PS2Keyboard::USB_HID[4 * 0xFF];
	constexpr uint32_t PS2Keyboard::ScanCode2_US_PS2_HID[256];
	constexpr uint32_t PS2Keyboard::ScanCode2_US_E0_HID[2 * 256];


	bool CapsLock = false;
	bool IsShifting = false;


	PS2Keyboard::PS2Keyboard()
	{
		using PSC = HAL::Devices::PS2::PS2Controller;
		HAL::Devices::PS2::PS2Controller();


		inb(PSC::DataPort);




		this->CharBuffer = new Library::Types::Queue();
		this->ByteBuffer = new Library::Types::Queue();
	}
	uint64_t PS2Keyboard::ReadChar()
	{
		return CharBuffer->Size() ? (uint64_t)CharBuffer->Pop() : 0;
	}

	uint64_t PS2Keyboard::Translate(uint64_t ScanCode, bool IsE0, bool IsF0)
	{
		uint64_t USBHIDIndex = IsE0 ? ScanCode2_US_E0_HID[ScanCode] : ScanCode2_US_PS2_HID[ScanCode] + 3;
		uint64_t findex = USBHIDIndex * 4 + 1;

		if(IsShifting)
			findex += 1;

		else if(CapsLock)
		{
			if(USB_HID[findex] >= 'a' && USB_HID[findex] <= 'z')
			{
				findex += 1;
			}
		}

		return USB_HID[findex];
	}

	void PS2Keyboard::HandleKeypress()
	{
		using PSC = HAL::Devices::PS2::PS2Controller;

		while(!(inb(PSC::CommandPort) & (1 << 0)))
			Multitasking::Yield();


		PSC::Device1Buffer = inb(PSC::DataPort);
		if(PSC::Device1Buffer == 0xFA || PSC::Device1Buffer == 0xFE)
			return;

		ByteBuffer->Push((void*)(uint64_t)PSC::Device1Buffer);


		if(PSC::Device1Buffer != 0xE1 && PSC::Device1Buffer != 0xF0 && PSC::Device1Buffer != 0xE0)
			this->DecodeScancode();
	}

	void PS2Keyboard::DecodeScancode()
	{
		using PSC = HAL::Devices::PS2::PS2Controller;
		uint8_t buf = (uint8_t)(uint64_t)ByteBuffer->Pop();

		bool IsE0 = false, IsF0 = false;

		if(buf == 0xE0)
		{
			IsE0 = true;
			buf = (uint8_t)(uint64_t)ByteBuffer->Pop();

			if(buf == 0xF0)
			{
				buf = (uint8_t)(uint64_t)ByteBuffer->Pop();
				IsF0 = true;
			}
		}
		else if(buf == 0xF0)
		{
			IsF0 = true;
			buf = (uint8_t)(uint64_t)ByteBuffer->Pop();
		}


		// TODO: move elsewhere
		// TODO: we'll eventually need a 'console buffer' of some description.
		// Since we're already in graphics mode, a 'graphics augmented' console sounds like a neat idea.
		// In addition to the planned windowing mode, of course.

		// TODO: Adopt a 'position on keyboard' kind of system.
		// AKA intepret the keyboard as a grid of keys, provide coordinates instead of scancodes.


		// FIXME: caps lock should only act on alphabets.




		if(!IsF0 && !IsE0 && (buf == 0x59 || buf == 0x12))
		{
			IsShifting = true;
			return;
		}

		if(IsF0 && !IsE0 && (buf == 0x59 || buf == 0x12))
		{
			IsShifting = false;
			return;
		}


		if(!IsF0 && !IsE0 && buf == 0x58)
		{
			CapsLock = !CapsLock;

			// send LED state


			while(inb(PSC::CommandPort) & (1 << 1));
			outb(PSC::DataPort, CapsLock & (1 << 2));

			return;
		}



		// if(IsE0 && !IsF0 && buf == 0x1F);

		if(!IsF0 && !IsE0 && this->State)
		{
			CharBuffer->Push((void*)((uint64_t)HAL::Drivers::PS2Keyboard::Translate(PSC::Device1Buffer)));
		}
	}
}
}
}
