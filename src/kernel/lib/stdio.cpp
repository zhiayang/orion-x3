// Stdio.c
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.


// Implements standard I/O functions.



#include <stdlib.h>
#include <stdarg.h>


using namespace Library;
using namespace Kernel::HAL::Devices;


typedef __builtin_va_list va_list;

bool AreInterruptsOn = false;
extern "C" void _STI()
{
	if(!AreInterruptsOn)
	{
		AreInterruptsOn = true;
		asm volatile("sti");
	}
}

extern "C" void _CLI()
{
	if(AreInterruptsOn)
	{
		AreInterruptsOn = false;
		asm volatile("cli");
	}
}

void operator delete(void* p)
{
	Kernel::HAL::DMem::FreeChunk((uint64_t*)p);
}

void operator delete[](void* p)
{
	Kernel::HAL::DMem::FreeChunk((uint64_t*)p);
}

extern "C" int __cxa_guard_acquire(Kernel::__cxxabiv1::__guard *g)
{
	return !*(char *)(g);
}

extern "C" void __cxa_guard_release(Kernel::__cxxabiv1::__guard *g)
{
	*(char *)g = 1;
}

extern "C" void __cxa_guard_abort(Kernel::__cxxabiv1::__guard *)
{

}











uint8_t inb(unsigned short port)
{
	uint8_t ret;
	asm volatile("inb %1, %0" : "=a"(ret) : "Nd"(port));
	return ret;
}
uint16_t inw(unsigned short port)
{
	uint16_t ret;
	asm volatile("inw %1, %0" : "=a"(ret) : "Nd"(port));
	return ret;
}
uint32_t inl(unsigned short port)
{
	uint32_t ret;
	asm volatile("inl %1, %0" : "=a"(ret) : "Nd"(port));
	return ret;
}
void outb(unsigned short port, uint8_t val)
{
	asm volatile("outb %0, %1" :: "a"(val), "Nd"(port));
}
void outw(unsigned short port, uint16_t val)
{
	asm volatile("outw %0, %1" :: "a"(val), "Nd"(port));
}
void outl(unsigned short port, uint32_t val)
{
	asm volatile("outl %0, %1" :: "a"(val), "Nd"(port));
}





namespace Kernel
{
	bool AssertCondition(bool condition, const char* filename, uint64_t line, const char* function, const char* reason)
	{
		if(BOpt_UnLikely(condition))
			return 0;


		HaltSystem(ERR_AssertFailed, filename, line, function, reason);
		return 1;
	}


	void HaltSystem(const char* message, const char* filename, uint64_t line, const char* function, const char* reason)
	{
		Stdio::printk("\n\n%wERROR: %w%s%r\n%wReason%r: %w%s%r\n%w%s%r -- %wLine %w%d%r%w, in %w%s%r\n\n%wOrion-X3 has met an unresolvable error, and will now halt.",
			VC_Yellow, VC_Red, message, VC_DarkCyan, VC_Orange, reason == 0 ? "None" : reason, VC_Cyan, filename, VC_Silver, VC_Blue, line, VC_Silver, VC_Violet, function, VC_Silver);


		LFB::RefreshBuffer();
		UHALT();
	}


	uint64_t ReduceBinaryUnits(uint64_t bytes)
	{
		// Takes bytes as a argument, reduces it as much as possible.

		uint8_t UnitsArrayIndex = 0;

		while((bytes / 1024) > 0)
		{
			bytes = ((bytes + 1024 - 1) / 1024);
			UnitsArrayIndex++;
		}
		return UnitsArrayIndex;
	}

	uint64_t GetReducedMemory(uint64_t bytes)
	{
		// Takes bytes as a argument, reduces it as much as possible.

		while((bytes / 1024) > 0)
		{
			bytes = ((bytes + 1024 - 1) / 1024);
		}
		return bytes;
	}
}
































namespace Library {
namespace Stdio
{
	void PrintString(const char *string, int64_t length)
	{
		for(uint64_t i = 0; i < (length < 0 ? String::Length(string) : length); i++)
		{
			LFB::Console::PrintChar(string[i]);
		}
	}


	void PrintHex_NoPrefix(uint64_t n, bool ReverseEndianness)
	{
		int tmp;

		int i = 0;


		// Mask bits of variables to determine size, and therefore how many digits to print.

		if((n & 0xF000000000000000) == 0)
			if((n & 0xFF00000000000000) == 0)
				if((n & 0xFFF0000000000000) == 0)
					if((n & 0xFFFF000000000000) == 0)
						if((n & 0xFFFFF00000000000) == 0)
							if((n & 0xFFFFFF0000000000) == 0)
								if((n & 0xFFFFFFF000000000) == 0)
									if((n & 0xFFFFFFFF00000000) == 0)
										if((n & 0xFFFFFFFFF0000000) == 0)
											if((n & 0xFFFFFFFFFF000000) == 0)
												if((n & 0xFFFFFFFFFFF00000) == 0)
													if((n & 0xFFFFFFFFFFFF0000) == 0)
														if((n & 0xFFFFFFFFFFFFF000) == 0)
															if((n & 0xFFFFFFFFFFFFFF00) == 0)
																if((n & 0xFFFFFFFFFFFFFFF0) == 0)
																	i = 0;
																else
																	i = 4;
															else
																i = 8;
														else
															i = 12;
													else
														i = 16;
												else
													i = 20;
											else
												i = 24;
										else
											i = 28;
									else
										i = 32;
								else
									i = 36;
							else
								i = 40;
						else
							i = 44;
					else
						i = 48;
				else
					i = 52;
			else
				i = 56;
		else
			i = 60;


		if(!ReverseEndianness)
		{
			for(; i >= 0; i -= 4)
			{
				tmp = (n >> i) & 0xF;


				if(tmp >= 0xA)
					LFB::Console::PrintChar(tmp - 0xA + 'A');

				else
					LFB::Console::PrintChar(tmp + '0');
			}
		}
		else
		{
			for(int z = 0; z <= i; z += 8)
			{
				tmp = (n >> z) & 0xFF;
				PrintHex_NoPrefix(tmp);
			}
		}
	}

	void PrintHex_Signed_NoPrefix(int64_t n)
	{
		// TODO: Print signed hex.
		PrintHex_NoPrefix(n);
	}



	void PrintHex(uint64_t n)
	{
		PrintString("0x");
		PrintHex_NoPrefix(n);
	}

	void PrintHex_Precision_NoPrefix(uint64_t n, int8_t leadingzeroes, bool ReverseEndianness)
	{
		if(leadingzeroes < 0)
		{
			return PrintHex_NoPrefix(n);
		}

		int tmp;


		int i = (leadingzeroes * 4) - 4;

		if(!ReverseEndianness)
		{
			for(; i >= 0; i -= 4)
			{
				tmp = (n >> i) & 0xF;

				if(tmp >= 0xA)
					LFB::Console::PrintChar(tmp - 0xA + 'A');

				else
					LFB::Console::PrintChar(tmp + '0');
			}
		}
		else
		{
			for(int z = 0; z <= i; z += 8)
			{
				tmp = (n >> z) & 0xFF;
				PrintHex_NoPrefix(tmp);
			}
		}
	}

	void PrintHex_Precision(uint64_t n, uint8_t leadingzeroes)
	{
		PrintString("0x");
		PrintHex_Precision_NoPrefix(n, leadingzeroes);
	}









	void PrintInteger(uint64_t num, int8_t Width)
	{
		PrintInteger_Signed(num, Width);
	}

	void PrintInteger_Signed(int64_t num, int8_t Width)
	{
		if(num == 0)
		{
			if(Width != -1)
			{
				for(int g = 0; g < Width; g++)
					PrintString("0");
			}
			else
				PrintString("0");

			return;
		}

		if(Width != -1)
		{
			uint64_t n = Math::AbsoluteValue(num);
			uint8_t k = 0;
			while(n > 0)
			{
				n /= 10;
				k++;
			}

			while(Width > k)
			{
				PrintString("0");
				k++;
			}
		}
		PrintString(Utility::ConvertToString(num));
	}

	void PrintBinary(uint64_t n)
	{
		// Number of bits to print.
		// Similiar 'tech' to puthex.

		unsigned int i = 0;
		if((n & 0xFFFFFFFF00000000) == 0)
		{
			if((n & 0xFFFF0000) == 0)
			{
				if((n & 0xFF00) == 0)
				{
					if((n & 0xF0) == 0)
					{
						i = 4;
					}
					else
					{
						i = 8;
					}
				}
				else
				{
					i = 16;
				}
			}
			else
			{
				i = 32;
			}
		}
		else
		{
			i = 64;
		}


		uint32_t bits[64];

		// Populate with invalid values (neither one nor zero) to tell us when to start.
		for(int f = 0; f < (64 - i); f++)
		{
			bits[f] = 0;
		}

		for(int f = i; f > 0; f--)
		{
			bits[f] = (n & (1 << f));
		}

		for(int f = i - 1; f >= 0; f--)
		{
			if(bits[f] == 0)
				LFB::Console::PrintChar('0');

			else
				LFB::Console::PrintChar('1');
		}
	}


	uint8_t PrintFloat(double fl, int8_t precision = 15)
	{
		if(precision < 0)
		{
			precision = 15;
		}

		// Put integer part first
		PrintInteger_Signed((uint64_t)fl);

		if((uint64_t)fl == fl)
			return 0;

		LFB::Console::PrintChar('.');

		// Get decimal part
		fl -= (uint64_t)fl;

		// return 0;

		uint32_t digit = 0;
		while(fl > 0 && precision > 0)
		{
			fl *= 10;

			if(precision == 1)
				digit = (uint32_t)Math::Round(fl);

			else
				digit = (uint32_t)fl;

			if(!(digit + '0' >= '0' && digit + '0' <= '9'))
			{
				LFB::Console::PrintChar('0');
				return 0;
			}

			LFB::Console::PrintChar(digit + '0');
			precision--;
			fl -= digit;
		}
		// Return the remaining number -- handle in printk() to print trailing zeroes
		return precision;
	}

	void PrintGUID(uint64_t High64, uint64_t Low64)
	{
		printk("%#8x-%#4x-%#4x-%#4x-%#12x", High64 & 0x00000000FFFFFFFF, High64 & 0x0000FFFF00000000, ((High64 & 0xFFFF000000000000) >> 48), (((Low64 & 0xFFFF) & 0xFF00) >> 8) | (((Low64 & 0xFFFF) & 0xFF) << 8), (((Low64 & 0xFF00000000000000) >> 56) | ((Low64 & 0x00FF000000000000) >> 40) | ((Low64 & 0x0000FF0000000000) >> 24) | ((Low64 & 0x000000FF00000000) >> 8) | ((Low64 & 0x00000000FF000000) << 8) | ((Low64 & 0x0000000000FF0000) << 24)));
	}









	extern "C" void SC_PrintChar(const char c)
	{
		LFB::Console::PrintChar(c);
	}
	extern "C" void SC_PrintInt(uint64_t x)
	{
		Library::Stdio::PrintInteger(x);
	}

	char* printk_sl(const char* string, ...)
	{
		return (char*)"";
	}

	void printk(const char* string, ...)
	{
		// Note:
		/*
			This is totally non-standard printf.

			For example, many of the other width specifiers etc are not implemented.
			Also, %q, %w, %r and %k are used for colour control.

			%x and %X do not behave as expected; %x prints in caps always, %X prints without the '0x' in front.


		*/

		bool IsFormat = false;
		bool DisplaySign = false;
		bool OmitZeroX = false;
		bool IsParsingPrecision = false;
		bool IsParsingWidth = false;
		bool ReverseHexEndianness = false;



		uint32_t WordColour = 0xFFFFFFFF;
		uint32_t ForeverColour = 0xFFFFFFFF;
		uint32_t CharColour = 0xFFFFFFFF;

		int8_t Precision = -1;
		int8_t Width = -1;

		va_list args;
		va_start(args, string);

		char c = 0;
		int z = 0;
		uint64_t x = 0;
		double f = 0.00;
		char* s = 0;
		char ch = 0;
		bool b = false;

		int length = String::Length(string);

		// %[parameter][flags][width][.precision][length]type

		for(int i = 0; i < length; i++)
		{
			c = string[i];

			if(!IsFormat && c == '%')
			{
				IsFormat = true;
				continue;
			}
			if(IsFormat)
			{
				switch(c)
				{
					case '%':
						LFB::Console::PrintChar('%');
						break;

					// Standard, parameter types

					case 'd':
					case 'i':
						z = va_arg(args, int32_t);
						if(DisplaySign)
						{
							if(z > 0)
								LFB::Console::PrintChar('+');

							DisplaySign = false;
						}
						PrintInteger_Signed(z, Width);
						break;

					case 's':
						s = va_arg(args, char*);
						PrintString(s, Precision);
						break;

					case 'u':
						x = va_arg(args, unsigned int);
						PrintInteger(x);
						break;

					// Also note here 'x' and 'X' behaviour is identical.
					case 'X':
					case 'x':
						x = va_arg(args, uint64_t);

						if(OmitZeroX)
						{
							PrintHex_Precision_NoPrefix(x, Width);
						}
						else
						{
							PrintHex_Precision(x, Width);
						}
						break;

					case 'c':
						ch = va_arg(args, int);
						LFB::Console::PrintChar(ch);
						break;


					case 'f':
					case 'F':
						f = va_arg(args, double);
						if(Precision > 0)
						{
							uint8_t remaining = PrintFloat(f, Precision);
							for(; remaining > 0; remaining--)
								LFB::Console::PrintChar('0');
						}
						else
						{
							PrintFloat(f);
						}
						break;


					// Binary
					case 'y':
						x = va_arg(args, uint64_t);
						PrintString(Library::Utility::ConvertToString(x));
						break;


					// Octal
					case 'o':
						x = va_arg(args, uint64_t);
						PrintString(Library::Utility::ConvertToString(x));
						break;


					case 'b':
						b = va_arg(args, int);
						PrintString(b ? "true" : "false");
						break;



					// Flags

					case '+':
						DisplaySign = true;
						IsFormat = true;
						continue;

					// Note this is the reverse of standard behaviour; we'll print '0x' every time unless # is specified.
					case '#':
						OmitZeroX = true;
						IsFormat = true;
						continue;

					case '^':
						ReverseHexEndianness = true;
						IsFormat = true;
						continue;


					// Custom stuff

					case 'q':
						// Change colour for the next word (up to a non-alphabetical character)
						WordColour = va_arg(args, uint32_t);
						LFB::Console::SetColour(WordColour);
						break;

					case 'w':
						// Change colour for the statement until changed back
						ForeverColour = va_arg(args, uint32_t);
						LFB::Console::SetColour(ForeverColour);
						break;

					case 'k':
						// Change colour for the next character only
						CharColour = va_arg(args, uint32_t);
						LFB::Console::SetColour(CharColour);
						break;

					case 'r':
						// Reset all colours to white
						CharColour = 0xFFFFFFFF;
						WordColour = 0xFFFFFFFF;
						ForeverColour = 0xFFFFFFFF;
						LFB::Console::SetColour(CharColour);
						break;



					// Width/precision
					case '.':
						IsParsingPrecision = true;
						IsFormat = true;
						continue;

					default:
						if(IsParsingPrecision)
						{
							char* prec = new char[3];
							Memory::Set(prec, 0, 3);

							int z = 0, f = i;
							for(z = 0, f = i; ((string[f] >= '0') && (string[f] <= '9')) || string[f] == '*'; z++, f++)
							{
								prec[z] = string[f];
							}
							if(prec[0] == '*')
								Precision = va_arg(args, uint64_t);

							else
								Precision = Utility::ConvertToInt(prec, 10);

							// -1 because continue; increments i
							i = f - 1;
							IsFormat = true;
							IsParsingPrecision = false;

							delete prec;
							continue;
						}
						else
						{
							char* width = new char[3];
							Memory::Set(width, 0, 3);

							int z = 0, f = i;
							for(z = 0, f = i; ((string[f] >= '0') && (string[f] <= '9')) || string[f] == '*'; z++, f++)
							{
								width[z] = string[f];
							}

							if(width[0] == '*')
								Width = va_arg(args, uint64_t);

							else
								Width = Utility::ConvertToInt(width, 10);

							// -1 because continue; increments i
							i = f - 1;
							IsFormat = true;

							delete width;

							continue;
						}
						break;
				}
				IsFormat = false;
				DisplaySign = false;
				OmitZeroX = false;

				Precision = -1;
				Width = -1;
				IsParsingPrecision = false;
				IsParsingWidth = false;
			}
			else
			{
				LFB::Console::PrintChar(c);
				if(WordColour != 0xFFFFFFFF && !(c >= 65 && c <= 122))
				{
					WordColour = 0xFFFFFFFF;
					LFB::Console::SetColour(WordColour);
				}
				if(CharColour != 0xFFFFFFFF)
				{
					CharColour = 0xFFFFFFFF;
					LFB::Console::SetColour(CharColour);
				}
			}
		}
		va_end(args);
		LFB::Console::SetColour(0xFFFFFFFF);
	}

} // namespace stdio
} // namespace library



















