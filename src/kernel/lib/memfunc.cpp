// MemFunc.cpp
// Copyright (c) 2011-2013, <zhiayang@gmail.com>
// Licensed under Creative Commons Attribution 3.0 Unported.


#include <stdlib.h>


// Memory Functions

extern "C" void* memcpy(void* dst, const void* src, unsigned long l)
{
	return Library::Memory::Copy(dst, src, l);
}
extern "C" void* memset(void* dst, int v, unsigned long l)
{
	return Library::Memory::Set(dst, v, l);
}


namespace Library
{
	namespace Memory
	{
		void* Set(void* destptr, int value, size_t length)
		{
			uint8_t* dest = (uint8_t*)destptr;

			for(size_t i = 0; i < length; i++)
				dest[i] = value & 0xFF;

			return destptr;
		}


		// Fill memory with given 4-byte value val. For easy
		// implementation, len is vetoed to be a multiple of 8

		void* Set32(void *dst, uint32_t val, unsigned long len)
		{
			uintptr_t d0 = 0;
			uint64_t uval = ((uint64_t)val << 32) + val;
			asm volatile(
				"rep stosq"
				:"=&D" (d0), "+&c" (len)
				:"0" (dst), "a" (uval)
				:"memory");

			return dst;
		}


		// Fill memory with given 8-byte value val. For easy
		// implementation, len is vetoed to be a multiple of 8

		void* Set64(void *dst, uint64_t val, unsigned long len)
		{
			uintptr_t d0 = 0;

			asm volatile(
				"rep stosq"
				:"=&D" (d0), "+&c" (len)
				:"0" (dst), "a" (val)
				:"memory");

			return dst;
		}







		void* Copy(void *dest, const void *src, unsigned long len)
		{

			asm volatile("cld; rep movsb"
			: "+c" (len), "+S" (src), "+D" (dest)
			:: "memory");


			return dest;
		}

		void* Copy32(void *dest, const void *src, unsigned long len)
		{
			asm volatile("cld; rep movsd"
			: "+c" (len), "+S" (src), "+D" (dest)
			:: "memory");


			return dest;
		}

		void* Copy64(void *dest, const void *src, unsigned long len)
		{
			asm volatile("cld; rep movsq"
			: "+c" (len), "+S" (src), "+D" (dest)
			:: "memory");


			return dest;
		}


		void* CopyOverlap(void* dst, const void* src, unsigned long n)
		{
			unsigned char *a = (unsigned char*)dst;
			const unsigned char *b = (unsigned char*)src;
			if(a <= b || b >= (a + n))
			{
				// No overlap, use memcpy logic (copy forward)
				Copy(dst, src, n);
			}
			else
			{
				asm volatile("std");
				Copy(dst, src, n);
				asm volatile("cld");
			}
			return dst;
		}

		void* CopyOverlap32(void* dst, const void* src, unsigned long n)
		{
			unsigned char *a = (unsigned char*)dst;
			const unsigned char *b = (unsigned char*)src;
			if(a <= b || b >= (a + n))
			{
				// No overlap, use memcpy logic (copy forward)
				Copy32(dst, src, n);
			}
			else
			{
				asm volatile("std");
				Copy32(dst, src, n);
				asm volatile("cld");
			}
			return dst;
		}

		void* CopyOverlap64(void* dst, const void* src, unsigned long n)
		{
			unsigned char *a = (unsigned char*)dst;
			const unsigned char *b = (unsigned char*)src;
			if(a <= b || b >= (a + n))
			{
				// No overlap, use memcpy logic (copy forward)
				Copy64(dst, src, n);
			}
			else
			{
				asm volatile("std");
				Copy64(dst, src, n);
				asm volatile("cld");
			}
			return dst;
		}
	}
}


